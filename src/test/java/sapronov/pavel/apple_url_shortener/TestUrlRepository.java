package sapronov.pavel.apple_url_shortener;

import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class TestUrlRepository {

    private UrlRepository target;

    @Before
    public void setUp() {
        target = new UrlRepository();
    }

    @Test
    public void testGetShortenedUrlNewUrl() {
        assertThat(target.getShortenedUrl("FirstUrl"), equalTo(1L));
    }

    @Test
    public void testGetShortenedUrlNewTwice() {
        assertThat(target.getShortenedUrl("FirstUrl"), equalTo(1L));
        assertThat(target.getShortenedUrl("FirstUrl"), equalTo(1L));
    }

    @Test
    public void testGetShortenedUrlTwoUrls() {
        assertThat(target.getShortenedUrl("FirstUrl"), equalTo(1L));
        assertThat(target.getShortenedUrl("FirstUrl"), equalTo(1L));
        assertThat(target.getShortenedUrl("SecondUrl"), equalTo(2L));
        assertThat(target.getShortenedUrl("SecondUrl"), equalTo(2L));
    }

    @Test
    public void testResolveShortcut() {
        Long shortCut = target.getShortenedUrl("FirstUrl");
        assertThat(target.resolveShortcut(shortCut), equalTo(Optional.of("FirstUrl")));
    }

    @Test
    public void testResolveShortcutReturnsEmptyString() {
        assertThat(target.resolveShortcut(999L), equalTo(Optional.empty()));
    }

}
