package sapronov.pavel.apple_url_shortener;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Optional;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ShortenerController.class)
public class TestShortenerController {

    private ShortenerController target;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UrlRepository repoMock;

    @After
    public void tearDown() {
        Mockito.reset(repoMock);
    }

    @Test
    public void testShortenUrl() throws Exception {
        given(repoMock.getShortenedUrl(anyString())).willReturn(1L);

        MvcResult mvcResult = mvc.perform(post("/").contentType(MediaType.APPLICATION_JSON)
                                                   .accept(MediaType.TEXT_HTML)
                                                   .content("{\"url\" : \"http://google.com\"}"))
                                 .andExpect(status().isOk()).andReturn();
        assertThat(mvcResult.getResponse().getContentAsString(), equalTo("1"));
        verify(repoMock).getShortenedUrl("http://google.com");
    }

    @Test
    public void testShortenUrlFailIncorrectUrl() throws Exception {
        given(repoMock.getShortenedUrl(anyString())).willReturn(1L);

        MvcResult mvcResult = mvc.perform(post("/").contentType(MediaType.APPLICATION_JSON)
                                                   .accept(MediaType.TEXT_HTML)
                                                   .content("{\"url\" : \"incorrectUrl\"}"))
                                 .andExpect(status().is4xxClientError()).andReturn();
        assertThat(mvcResult.getResponse().getContentAsString(), equalTo("You've entered Incorrect url."));
        verify(repoMock, times(0)).getShortenedUrl(anyString());
    }

    @Test
    public void testResolveShortcut() throws Exception {
        given(repoMock.resolveShortcut(1L)).willReturn(Optional.of("http://google.com"));

        MvcResult mvcResult = mvc.perform(get("/s/1"))
                                 .andExpect(status().is3xxRedirection()).andReturn();
        assertThat(mvcResult.getResponse().getHeader("location"), equalTo("http://google.com"));
        verify(repoMock).resolveShortcut(1L);
    }

    @Test
    public void testResolveShortcutFailShortcutIsNotExisted() throws Exception {
        given(repoMock.resolveShortcut(anyLong())).willReturn(Optional.empty());

        MvcResult mvcResult = mvc.perform(get("/s/1"))
                                 .andExpect(status().is3xxRedirection()).andReturn();
        assertThat(mvcResult.getResponse().getHeader("location"), containsString("shortcut_not_found.html"));
        verify(repoMock).resolveShortcut(anyLong());
    }

    @Test
    public void testResolveShortcutFailToBigNumberForShortcut() throws Exception {
        MvcResult mvcResult = mvc.perform(get("/s/9223372036854775808"))
                                 .andExpect(status().is3xxRedirection()).andReturn();
        assertThat(mvcResult.getResponse().getHeader("location"), containsString("shortcut_not_found.html"));
        verify(repoMock, times(0)).resolveShortcut(anyLong());
    }

}
