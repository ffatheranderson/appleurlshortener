$(document).ready(function () {

    $("#makeShort").click(function () {
        tryToSubmitUrl();
    });

    $("#urlField").on('keyup', function (e) {
        if (e.keyCode == 13) {
            tryToSubmitUrl();
        }
    });

    function tryToSubmitUrl() {
        var urlText = $("#urlField").val().trim();
        if (urlText.length == 0)
            return;

        $.ajax("/", {
            method: "POST",
            contentType: "application/json",
            data: JSON.stringify({url: urlText})
        }).done(function (data) {
            showShortcut(data);
        }).fail(function (data, status) {
            showError(status + ": " + data.responseText);
        });
    }

    function showShortcut(shortcut) {
        var resultDiv = $("#result");
        resultDiv.empty();
        var div = $("<div>Here is your shortcut: </div>");
        var href = window.location.href + 's/' + shortcut;
        var a = $("<a></a>");
        a.text(href);
        a.attr('href', href);
        a.appendTo(div);
        div.appendTo(resultDiv);
    }

    function showError(message) {
        var resultDiv = $("#result");
        resultDiv.empty();
        var div = $("<div style='color: red;'></div>");
        div.text(message);
        div.appendTo(resultDiv);
    }

});
