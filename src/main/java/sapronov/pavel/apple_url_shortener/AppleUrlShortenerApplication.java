package sapronov.pavel.apple_url_shortener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppleUrlShortenerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppleUrlShortenerApplication.class, args);
    }

}
