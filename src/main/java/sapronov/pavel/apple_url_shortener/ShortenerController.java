package sapronov.pavel.apple_url_shortener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

@RestController
public class ShortenerController {

    @Autowired
    private UrlRepository repo;

    private static final String SHORTCUT_NOT_FOUND_PAGE = "/shortcut_not_found.html";
    private static final String INCORRECT_URL_MSG = "You've entered Incorrect url.";

    @GetMapping("/s/{shortcut}")
    public RedirectView resolveShortcut(@PathVariable String shortcut) {
        try {
            Long shortCut = Long.valueOf(shortcut);

            Optional<String> mayBeUrl = repo.resolveShortcut(shortCut);

            return new RedirectView(mayBeUrl.isPresent() ? mayBeUrl.get() : SHORTCUT_NOT_FOUND_PAGE);
        } catch (NumberFormatException nfe) {
            return new RedirectView(SHORTCUT_NOT_FOUND_PAGE);
        }
    }

    @PostMapping(path = "/", produces = MediaType.TEXT_HTML_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> shortenUrl(@RequestBody UrlEntity urlE) {
        String url = urlE.getUrl();

        if (isValidUrl(url))
            return ResponseEntity.ok().body(String.valueOf(repo.getShortenedUrl(url)));

        return ResponseEntity.badRequest().body(INCORRECT_URL_MSG);
    }

    private boolean isValidUrl(String str) {
        try {
            new URL(str);
            return true;
        } catch (MalformedURLException e) {
            return false;
        }
    }

}
