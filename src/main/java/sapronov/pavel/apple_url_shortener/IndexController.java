package sapronov.pavel.apple_url_shortener;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @GetMapping("/")
    public String indexHtml() {
        return "index.html";
    }

}
