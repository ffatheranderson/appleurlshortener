package sapronov.pavel.apple_url_shortener;

import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class UrlRepository {

    private final Map<String, Long> URLS_TO_SHORTCUTS = new ConcurrentHashMap<>();
    private final Map<Long, String> SHORTCUTS_TO_URLS = new ConcurrentHashMap<>();
    private AtomicLong counter = new AtomicLong();

    /**
     * Resolves the shortcut to original url.
     *
     * @param shortCut
     * @return original url or empty string in case if shortcut is not existed.
     */
    public Optional<String> resolveShortcut(Long shortCut) {
        return Optional.ofNullable(SHORTCUTS_TO_URLS.get(shortCut));
    }

    /**
     * Returns shortcut for the original url if shortcut is existed or creates a new one if not.
     *
     * @param url
     * @return shortcut for the url.
     */
    public Long getShortenedUrl(String url) {
        Long s = URLS_TO_SHORTCUTS.get(url);

        if (s == null)
            synchronized (this) {
                s = URLS_TO_SHORTCUTS.get(url);
                if (s == null) {
                    s = counter.incrementAndGet();
                    URLS_TO_SHORTCUTS.put(url, s);
                    SHORTCUTS_TO_URLS.put(s, url);
                }
            }
        return s;
    }
}